﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Network
{
    public class NetworkConfig
    {
        public const int bufferSize = 1024 * 4;
        public const int baseServerSocketArgsCount = 1024 * 32;
        public const int baseClientSocketArgsCount = 128;

        public const int serverListenPort = 10000;
    }
}
