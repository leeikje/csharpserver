﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace Common.Network
{
    public class SocketAsyncEventArgsPool
    {
        Stack<SocketAsyncEventArgs> m_pool;

        public SocketAsyncEventArgsPool(int capacity)
        {
            m_pool = new Stack<SocketAsyncEventArgs>(capacity);
            lock (m_pool)
            {
                for(int i=0; i< capacity; ++i)
                {
                    var item = new SocketAsyncEventArgs();
                    item.UserToken = new byte[NetworkConfig.bufferSize];
                    m_pool.Push(item);
                }                
            }            
        }

        public SocketAsyncEventArgs GetItem()
        {
            lock(m_pool)
            {
                if (m_pool.Count > 0)
                    return m_pool.Pop();
                else
                {
                    var item = new SocketAsyncEventArgs();
                    item.UserToken = new byte[NetworkConfig.bufferSize];

                    return item;
                }
            }
        }

        public void ReleaseItem(SocketAsyncEventArgs item)
        {
            if (item == null) { throw new ArgumentNullException("Items added to a SocketAsyncEventArgsPool cannot be null"); }
            
            lock (m_pool)
            {
                m_pool.Push(item);
            }
        }
    }
}
