﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Common.Network
{
    public interface IBaseClientOwner
    {
        void ConnectSession();
        void DisconnectSession();
        void HandlePacket(dynamic packet);
    }

    public class BaseClient : ISessionOwner
    {
        IBaseClientOwner m_baseClientOwner;
        BaseSession m_session;

        public SocketAsyncEventArgsPool SocketAsyncEventArgsPool { get; set; }

        public BaseClient(IBaseClientOwner owner)
        {
            m_baseClientOwner = owner;
            m_session = null;
            SocketAsyncEventArgsPool = new SocketAsyncEventArgsPool(NetworkConfig.baseClientSocketArgsCount);
        }
        
        public void CloseSession(BaseSession session)
        {
            m_baseClientOwner.DisconnectSession();
            m_session.Release();
        }

        public void ForceClose()
        {
            m_session.ForceClose();
        }
        
        public bool Connect(IPEndPoint theHostEndPoint)
        {
            IPEndPoint hostEndPoint = theHostEndPoint;

            m_session = new BaseSession();
            m_session.Capture(new Socket(hostEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp), this);

            bool isConnection = m_session.Connect(hostEndPoint);
            if (isConnection)
            {
                m_baseClientOwner.ConnectSession();
                m_session.StartReceive();
            }

            return isConnection;
        }


        public void HandleStream(int sessionID, byte[] stream)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream packetStream = new MemoryStream();
            packetStream.Write(stream, 0, stream.Length);
            packetStream.Seek(0, SeekOrigin.Begin);

            m_baseClientOwner.HandlePacket(bf.Deserialize(packetStream));
        }

        public void SendPacket(dynamic packet)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream packetStream = new MemoryStream();
            bf.Serialize(packetStream, packet);

            m_session.SendStream(packetStream.ToArray());
        }
    }
}
