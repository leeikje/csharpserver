﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;

using System.Net;

namespace Common.Network
{
    public interface ISessionOwner
    {
        SocketAsyncEventArgsPool SocketAsyncEventArgsPool { get; set; }
        void CloseSession(BaseSession session);
        void HandleStream(int sessionID, byte[] stream);
    }

    public class BaseSession
    {
        static int sessionIDGenerator = 0;
        public int SessionID { get; set; }
        Socket m_socket;
        SocketAsyncEventArgs m_receiveSocketAsyncEventArgs;
        EventHandler<SocketAsyncEventArgs> m_socketAsyncEventArgsHandler;

        bool m_receiveContinuous;
        int m_receiveLength;
        int m_receiveTargetLength;
        byte[] m_receiveBuffer;

        ISessionOwner m_sessionOwner;

        public Socket Socket
        {
            get { return m_socket; }
        }
        SocketAsyncEventArgs CaptureSocketAsyncEventArgs()
        {
            var args = m_sessionOwner.SocketAsyncEventArgsPool.GetItem();            
            args.Completed += m_socketAsyncEventArgsHandler;

            return args;
        }
        void ReleaseSocketAsyncEventArgs(SocketAsyncEventArgs args)
        {
            args.Completed -= m_socketAsyncEventArgsHandler;
            m_sessionOwner.SocketAsyncEventArgsPool.ReleaseItem(args);
        }
        public BaseSession()
        {
            m_socket = null;
            m_socketAsyncEventArgsHandler = new EventHandler<SocketAsyncEventArgs>(IO_Completed);
            m_receiveBuffer = new byte[NetworkConfig.bufferSize];
        }

        void MakeSessionID()
        {
            SessionID = Interlocked.Increment(ref sessionIDGenerator);
        }

        public void ForceClose()
        {
            CloseClientSocket(null);
        }

        // init, set socket, session id
        public void Capture(Socket socket, ISessionOwner sessionOwner)
        {
            m_socket = socket;
            m_sessionOwner = sessionOwner;

            m_receiveContinuous = false;
            m_receiveTargetLength = 0;
            m_receiveLength = 0;

            m_receiveSocketAsyncEventArgs = CaptureSocketAsyncEventArgs();

            MakeSessionID();
        }

        public void Release()
        {
            ReleaseSocketAsyncEventArgs(m_receiveSocketAsyncEventArgs);

            m_sessionOwner = null;
            m_socket = null;
        }        

        public void IO_Completed(object sender, SocketAsyncEventArgs e)
        {
            switch (e.LastOperation)
            {
                case SocketAsyncOperation.Receive:
                    ProcessReceive(e);
                    break;
                case SocketAsyncOperation.Send:
                    ProcessSend(e);
                    break;                    

                case SocketAsyncOperation.Disconnect:
                    CloseClientSocket(e);

                    break;
                default:
                    throw new ArgumentException("The last operation completed on the socket was not a receive or send");
            }
        }

        public void StartReceive()
        {
            byte[] buffer = (byte[])m_receiveSocketAsyncEventArgs.UserToken;
            m_receiveSocketAsyncEventArgs.SetBuffer(buffer, 0, buffer.Length);

            bool willRaiseEvent = m_socket.ReceiveAsync(m_receiveSocketAsyncEventArgs);
            if (!willRaiseEvent)
            {
                ProcessReceive(m_receiveSocketAsyncEventArgs);
            }
        }
        int GetPacketLength(byte[] buffer, int currentIndex)
        {
            return BitConverter.ToInt32(buffer, currentIndex);
        }
        void SetPacketLength(byte[] buffer, int length)
        {
            BitConverter.GetBytes(length).CopyTo(buffer, 0);
        }
        bool MakeStreams(byte[] buffer, int bufferLength, Queue<byte[]> streams)
        {
            int currentIndex = 0;
            int getBufferSize = 0;
            do
            {
                if (!m_receiveContinuous && bufferLength - currentIndex < 4)
                    return false;

                if (!m_receiveContinuous)
                {
                    m_receiveTargetLength = GetPacketLength(buffer, currentIndex);
                    currentIndex += 4;

                    m_receiveLength = 0;

                    if (m_receiveBuffer.Length < m_receiveTargetLength)
                        m_receiveBuffer = new byte[m_receiveTargetLength * 2];
                }

                getBufferSize = m_receiveTargetLength - m_receiveLength;

                // 다 못받음.
                if (getBufferSize > bufferLength - currentIndex)
                {
                    getBufferSize = bufferLength - currentIndex;

                    Array.Copy(buffer, currentIndex, m_receiveBuffer, m_receiveLength, getBufferSize);
                    currentIndex += getBufferSize;
                    m_receiveLength += getBufferSize;
                    m_receiveContinuous = true;
                }
                // 다 받음.
                else
                {
                    Array.Copy(buffer, currentIndex, m_receiveBuffer, m_receiveLength, getBufferSize);
                    currentIndex += getBufferSize;

                    m_receiveLength = 0;
                    m_receiveTargetLength = 0;
                    m_receiveContinuous = false;

                    streams.Enqueue((byte[])m_receiveBuffer.Clone());
                }
            } while (currentIndex < bufferLength);

            return true;
        }

        void ProcessReceive(SocketAsyncEventArgs e)
        {
            if (e.SocketError != SocketError.Success || e.BytesTransferred == 0)
                CloseClientSocket(e);
            else
            {
                Queue<byte[]> streams = new Queue<byte[]>();
                if (!MakeStreams(e.Buffer, e.BytesTransferred, streams))
                    CloseClientSocket(e);

                foreach (var stream in streams)
                {
                    // handling 하자.
                    m_sessionOwner.HandleStream(SessionID, stream);
                }

                StartReceive();
            }
        }

        void ProcessSend(SocketAsyncEventArgs e)
        {
            if (e.SocketError != SocketError.Success)
                CloseClientSocket(e);

            ReleaseSocketAsyncEventArgs(e);
        }

        public void SendStream(byte[] stream)
        {
            var args = CaptureSocketAsyncEventArgs();
            byte[] buffer = (byte[])args.UserToken;
            if (buffer.Length < stream.Length + 4)
            {
                buffer = new byte[(stream.Length + 4) * 2];
                args.UserToken = buffer;
            }
            SetPacketLength(buffer, stream.Length);
            Array.Copy(stream, 0, buffer, 4, stream.Length);
            args.SetBuffer(buffer, 0, stream.Length + 4);            

            bool willRaiseEvent = m_socket.SendAsync(args);
            if (!willRaiseEvent)
            {
                ProcessSend(args);
            }
        }

        void CloseClientSocket(SocketAsyncEventArgs e)
        {
            if (m_socket == null)
                return;

            try
            {
                m_socket.Shutdown(SocketShutdown.Send);
            }
            catch (Exception) { }
            m_socket.Close();
            m_sessionOwner.CloseSession(this);
        }

        public bool Connect(IPEndPoint hostEndPoint)
        {
            bool connectedSuccessfully = false;

            try
            {
                m_socket.Connect(hostEndPoint);
                connectedSuccessfully = true;
            }
            catch (SocketException ex)
            {
                throw ex;
            }

            return connectedSuccessfully;
        }
    }
}
