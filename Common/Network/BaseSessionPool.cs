﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Network
{
    class BaseSessionPool
    {
        Stack<BaseSession> m_pool;

        public BaseSessionPool(int capacity)
        {
            m_pool = new Stack<BaseSession>(capacity);
            lock (m_pool)
            {
                for (int i = 0; i < capacity; ++i)
                    m_pool.Push(new BaseSession());
            }
        }

        public BaseSession GetItem()
        {
            lock (m_pool)
            {
                if (m_pool.Count > 0)
                    return m_pool.Pop();
                else
                    return new BaseSession();
            }
        }

        public void ReleaseItem(BaseSession item)
        {
            if (item == null) { throw new ArgumentNullException("Items added to a BaseSessionPool cannot be null"); }

            lock (m_pool)
            {
                m_pool.Push(item);
            }
        }
    }
}
