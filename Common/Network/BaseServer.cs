﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Common.Network
{
    public interface IBaseServerOwner
    {
        void AcceptSession(int sessionID);
        void CloseSession(int sessionID);
        void HandlePacket(int sessionID, dynamic packet);
    }

    public class BaseServer : ISessionOwner
    {
        public SocketAsyncEventArgsPool SocketAsyncEventArgsPool { get; set; }

        BaseSessionPool m_baseSessionPool;
        Dictionary<int, BaseSession> m_sessions;
        IBaseServerOwner m_baseServerOwner;

        int m_numConnections;                
        Semaphore m_maxNumberAcceptedClients;

        Socket listenSocket;
        
        int m_numConnectedSockets;

        public BaseServer(int numConnections, IBaseServerOwner owner)
        {
            m_baseServerOwner = owner;
            m_numConnections = numConnections;
            m_baseSessionPool = new BaseSessionPool(numConnections);
            m_maxNumberAcceptedClients = new Semaphore(m_numConnections, m_numConnections);
            m_sessions = new Dictionary<int, BaseSession>();
            SocketAsyncEventArgsPool = new SocketAsyncEventArgsPool(NetworkConfig.baseServerSocketArgsCount);
            m_numConnectedSockets = 0;
        }

        public void Start(IPEndPoint localEndPoint)
        {
            listenSocket = new Socket(localEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            listenSocket.Bind(localEndPoint);
            listenSocket.Listen(NetworkConfig.serverListenPort);
            StartAccept(null);
        }
        
        public void StartAccept(SocketAsyncEventArgs acceptEventArg)
        {
            if (acceptEventArg == null)
            {
                acceptEventArg = new SocketAsyncEventArgs();
                acceptEventArg.Completed += new EventHandler<SocketAsyncEventArgs>(AcceptEventArg_Completed);
            }
            else
            {
                // socket must be cleared since the context object is being reused
                acceptEventArg.AcceptSocket = null;
            }

            m_maxNumberAcceptedClients.WaitOne();
            bool willRaiseEvent = listenSocket.AcceptAsync(acceptEventArg);
            if (!willRaiseEvent)
            {
                ProcessAccept(acceptEventArg);
            }
        }
        
        void AcceptEventArg_Completed(object sender, SocketAsyncEventArgs e)
        {
            ProcessAccept(e);
        }

        private void ProcessAccept(SocketAsyncEventArgs e)
        {            
            AcceptSession(e.AcceptSocket);
            StartAccept(e);
        }

        public void AcceptSession(Socket s)
        {
            Interlocked.Increment(ref m_numConnectedSockets);

            var session = m_baseSessionPool.GetItem();
            session.Capture(s, this);
            m_sessions.Add(session.SessionID, session);

            m_baseServerOwner.AcceptSession(session.SessionID);
            session.StartReceive();
        }

        public void CloseSession(BaseSession session)
        {
            m_baseServerOwner.CloseSession(session.SessionID);
            m_sessions.Remove(session.SessionID);
            session.Release();

            m_baseSessionPool.ReleaseItem(session);

            Interlocked.Decrement(ref m_numConnectedSockets);
            m_maxNumberAcceptedClients.Release();
        }

        public void HandleStream(int sessionID, byte[] stream)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream packetStream = new MemoryStream();
            packetStream.Write(stream, 0, stream.Length);
            packetStream.Seek(0, SeekOrigin.Begin);
            
            m_baseServerOwner.HandlePacket(sessionID, bf.Deserialize(packetStream));
        }

        public void SendPacket(int sessionID, dynamic packet)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream packetStream = new MemoryStream();
            bf.Serialize(packetStream, packet);
            
            m_sessions[sessionID].SendStream(packetStream.ToArray());
        }
    }
}
