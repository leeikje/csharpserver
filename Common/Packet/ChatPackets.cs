﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Packet
{
    #region Data 정의
    [Serializable]
    public struct UserData
    {
        public string userName { get; set; }
        public List<string> roomNames { get; set; }
    }

    [Serializable]
    public struct ChatData
    {
        public string userName { get; set; }
        public string message { get; set; }
        public DateTime created_at { get; set; }
    }

    [Serializable]
    public struct RoomData
    {
        public string roomName { get; set; }
        public List<UserData> roomUsers { get; set; }
        public List<ChatData> roomChats { get; set; }

        public void EnterRoom(UserData user)
        {
            roomUsers.Add(user);
            user.roomNames.Add(roomName);
        }

        public void LeaveRoom(UserData user)
        {
            roomUsers.Remove(user);
            user.roomNames.Remove(roomName);
        }

        public void Chat(ChatData chatData)
        {
            roomChats.Add(chatData);
        }
    }
    #endregion

    #region Packet 정의
    [Serializable]
    public struct TextMessagePacket
    {
        public string msg { get; set; }
    }

    [Serializable]
    public struct CQ_LoginPacket
    {
        public string userName { get; set; }
    }

    [Serializable]
    public struct SA_LoginPacket
    {
        public string userName { get; set; }
        public int result { get; set; }
    }

    [Serializable]
    public struct CQ_RoomListPacket
    {        
    }

    [Serializable]
    public struct SN_RoomListPacket
    {
        public List<RoomData> roomList { get; set; }
        public List<RoomData> serverRoomList { get; set; }
    }

    [Serializable]
    public struct CQ_MakeRoomPacket
    {
        public string roomName { get; set; }
    }

    [Serializable]
    public struct SA_MakeRoomPacket
    {
        public RoomData room { get; set; }
        public int result { get; set; }
    }

    [Serializable]
    public struct CQ_EnterRoomPacket
    {
        public string roomName { get; set; }
    }

    [Serializable]
    public struct SA_EnterRoomPacket
    {
        public RoomData room { get; set; }
        public int result { get; set; }
    }

    [Serializable]
    public struct CQ_ChatPacket
    {
        public string roomName { get; set; }
        public string message { get; set; }
    }

    [Serializable]
    public struct SN_ChatPacket
    {
        public string roomName { get; set; }
        public ChatData chat { get; set; }
    }
    #endregion
}
