﻿CREATE TABLE [dbo].[Users]
(
	[Id] BIGINT NOT NULL PRIMARY KEY, 
    [name] NVARCHAR(30) NOT NULL 
)

GO


CREATE INDEX [IX_Users_name] ON [dbo].[Users] ([name])
