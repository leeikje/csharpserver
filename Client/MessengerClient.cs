﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using Common.Network;
using Common.Packet;

using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Client
{
    class MessengerClient : IBaseClientOwner
    {
        public bool IsConnecting { get; set; }
        BaseClient m_baseClient;

        public MessengerClient()
        {
            IsConnecting = false;
            m_baseClient = new BaseClient(this);
        }

        public bool ConnectServer()
        {
            IPAddress address = new IPAddress(new byte[] { 10, 60, 202, 14 });
            IPEndPoint serverPoint;
            serverPoint = new IPEndPoint(address, NetworkConfig.serverListenPort);

            IsConnecting = m_baseClient.Connect(serverPoint);

            return IsConnecting;
        }

        public void ConnectSession()
        {
            System.Console.WriteLine("ConnectSession");
        }
        public void DisconnectSession()
        {
            System.Console.WriteLine("DisconnectSession");
            IsConnecting = false;
        }

        private object lockObject = new object();

        public void HandlePacket(dynamic packet)
        {
            lock (lockObject)
            {
                HandlePacket(packet);
            }
        }

        public void HandlePacket(TextMessagePacket packet)
        {
            Console.WriteLine($"Handle Stream : {packet.msg}");
        }

        public void SendPacket(dynamic packet) { m_baseClient.SendPacket(packet); }

        public void SendMessage(string msg)
        {
            TextMessagePacket packet = new TextMessagePacket();
            packet.msg = msg;

            SendPacket(packet);
        }

        string GetRandomDigit(int length)
        {
            Random random = new Random();
            StringBuilder output = new StringBuilder();
            while (output.Length != length)
                output.Append(random.Next(0, 9));
            return output.ToString();
        }

        enum TestStatus { LoginWait, RoomDataWait, DoTest }
        TestStatus status;
        UserData userData;
        Dictionary<string, RoomData> rooms;
        List<RoomData> serverRooms;

        public void DoLogin()
        {
            userData = new UserData();
            userData.userName = GetRandomDigit(10); ;

            CQ_LoginPacket packet = new CQ_LoginPacket();
            packet.userName = userData.userName;
            SendPacket(packet);

            status = TestStatus.LoginWait;
        }

        public void LoginWait()
        {
            while (status <= TestStatus.LoginWait)
                Thread.Sleep(100);

            while (status <= TestStatus.RoomDataWait)
                Thread.Sleep(100);
        }

        public void HandlePacket(SA_LoginPacket packet)
        {
            if(packet.result >= 0)
            {
                status = TestStatus.RoomDataWait;
            }            
            else
            {
                Console.WriteLine($"SA_LoginPacket fail.. {packet.result}");
                m_baseClient.ForceClose();
            }
        }
        public void HandlePacket(SN_RoomListPacket packet)
        {
            rooms = new Dictionary<string, RoomData>();
            foreach(RoomData room in packet.roomList)
            {
                rooms.Add(room.roomName, room);
            }

            serverRooms = packet.serverRoomList;
            status = TestStatus.DoTest;
        }

        List<Action> actions;
        List<int> frequency;
        List<double> probability;
        Random random;

        public void InitTest()
        {
            actions = new List<Action>() { DoMakeRoom, DoEnterRoom, DoChat };
            frequency = new List<int>() { 1, 10, 1000 };
            probability = new List<double>();
            int i, sum;
            double ps;

            sum = frequency.Sum();
            ps = 0.0;
            for (i = 0; i < frequency.Count-1; ++i)
            {
                ps += frequency[i] / (double)sum;
                probability.Add(ps);
            }
            probability.Add(1.0);

            random = new Random();
        }

        public void DoTest()
        {
            int i;     
            lock (lockObject)
            {                
                double value = random.NextDouble();
                for(i=0; i<probability.Count;++i)
                    if(value <= probability[i])
                    {
                        actions[i]();
                        break;
                    }
            }
        }

        public void DoMakeRoom()
        {
            CQ_MakeRoomPacket packet= new CQ_MakeRoomPacket();
            packet.roomName = "roomName_" + GetRandomDigit(10);

            m_baseClient.SendPacket(packet);
        }

        public void DoEnterRoom()
        {
            if(rooms.Count > 0)
            {
                Random r = new Random();
                RoomData room = serverRooms[r.Next(serverRooms.Count)];

                if( !rooms.ContainsKey(room.roomName) )
                {
                    CQ_EnterRoomPacket packet = new CQ_EnterRoomPacket();
                    packet.roomName = room.roomName;
                    m_baseClient.SendPacket(packet);
                }
            }
            else
            {
                DoMakeRoom();
            }            
        }

        int messageSequence = 0;
        public void DoChat()
        {
            if(rooms.Count > 0)
            {
                Random rand = new Random();
                var room = rooms.ElementAt(rand.Next(0, rooms.Count)).Value;

                CQ_ChatPacket packet = new CQ_ChatPacket();
                packet.roomName = room.roomName;
                packet.message = $"Do Chat{userData.userName} : {messageSequence++}";
                m_baseClient.SendPacket(packet);
            }
            else
            {
                DoEnterRoom();
            }
        }

        public void HandlePacket(SA_MakeRoomPacket packet)
        {
            if(packet.result <= 0)
            {
                rooms.Add(packet.room.roomName, packet.room);
            }
            else
            {
                Console.WriteLine($"SA_MakeRoomPacket fail.. {packet.result}");
                m_baseClient.ForceClose();
            }
        }

        public void HandlePacket(SA_EnterRoomPacket packet)
        {
            if (packet.result <= 0)
            {
                rooms.Add(packet.room.roomName, packet.room);
            }
            else
            {
                Console.WriteLine($"SA_EnterRoomPacket fail.. {packet.result}");
                m_baseClient.ForceClose();
            }
        }

        public void HandlePacket(SN_ChatPacket packet)
        {
            RoomData room;
            if( rooms.TryGetValue(packet.roomName, out room))
            {
                room.roomChats.Add(packet.chat);
                Console.WriteLine($"{packet.chat.userName}\t::\t{packet.chat.message}");
                Console.WriteLine($"{packet.chat.created_at}");
            }
            else
            {
                Console.WriteLine("SN_ChatPacket fail... not exist room..");
            }
        }
    }
}
