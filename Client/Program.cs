﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Client
{
    class Program
    {
        static void RunBot()
        {
            MessengerClient messengerClient = new MessengerClient();

            messengerClient.ConnectServer();
            messengerClient.DoLogin();
            messengerClient.LoginWait();
            messengerClient.InitTest();

            while (messengerClient.IsConnecting)
            {
                messengerClient.DoTest();
            }
        }

        static void Main(string[] args)
        {
            Task[] bots = new Task[100];
            int i;

            for(i=0; i<100; ++i)
                bots[i] = Task.Factory.StartNew(RunBot);

            Task.WaitAll(bots);
        }
    }
}
