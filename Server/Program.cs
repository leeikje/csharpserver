﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Server
{
    public class TaskTest
    {
        List<int> threadIdList = new List<int>();
        object lockList = new object();

        void AddBag(int threadId)
        {
            lock (lockList)
            {
                if (!threadIdList.Contains(threadId))
                    threadIdList.Add(threadId);
            }
        }

        void Print()
        {
            Console.WriteLine("Thread Count : {0}, Thread IDs : {1}", threadIdList.Count, string.Join(",", threadIdList));
        }

        public void Run()
        {
            List<Task> taskList = new List<Task>();
            for (int i = 0; i < 100; i++)
            {
                Task t = new Task(() =>
                {
                    AddBag(Thread.CurrentThread.ManagedThreadId);
                    Thread.Sleep(500);
                });

                taskList.Add(t);
                t.Start();

            }
            taskList.ForEach(x => x.Wait());
            Print();
        }
    }
    class Program
    {
        static void PrintThreadInfo()
        {
            int minWorkerThreads = 0;
            int minCompletionPortThreads = 0;
            ThreadPool.GetMinThreads(out minWorkerThreads, out minCompletionPortThreads);

            int maxWorkerThreads = 0;
            int maxCompletionPortThreads = 0;
            ThreadPool.GetMaxThreads(out maxWorkerThreads, out maxCompletionPortThreads);

            Console.WriteLine("Min Worker Threads : {0}, max Worker Threads : {1}", minWorkerThreads, maxWorkerThreads);
            Console.WriteLine("Min CompletionPort Threads : {0}, max CompletionPort Threads : {1}", minCompletionPortThreads, maxCompletionPortThreads);
        }

        static void Main(string[] args)
        {
            //Program.PrintThreadInfo();
            //ThreadPool.SetMinThreads(50, 100);
            //var tt = new TaskTest();
            //tt.Run();
            MessengerServer ms = new MessengerServer();

            ms.Start();
            while (ms.IsRunning)
            {
                Console.WriteLine("running....");
                Thread.Sleep(10000);
            }
        }
    }
}
