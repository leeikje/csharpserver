﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections.Concurrent;

using Common.Network;
using Common.Packet;


namespace Server
{
    class MessengerServer : IBaseServerOwner
    {
        public bool IsRunning { get; set; }
        BaseServer m_baseServer;
        ConcurrentDictionary<int, User> m_users;
        ConcurrentDictionary<string, User> m_usersByName;
        ConcurrentDictionary<string, RoomData> m_rooms;
        static RedisManager RedisManager { get; set; }
        
        public MessengerServer()
        {
            RedisManager = new RedisManager();
            RedisManager.InitRedisManager("localhost");
        }

        public void Start()
        {
            m_users = new ConcurrentDictionary<int, User>();
            m_usersByName = new ConcurrentDictionary<string, User>();
            m_rooms = new ConcurrentDictionary<string, RoomData>();

            m_baseServer = new BaseServer(10000, this);
            m_baseServer.Start(new System.Net.IPEndPoint(System.Net.IPAddress.Any, NetworkConfig.serverListenPort));

            IsRunning = true;
        }
        public void AcceptSession(int sessionID)
        {
            var user = new User(sessionID);
            if( !m_users.TryAdd(sessionID, user) )
            {
                Console.WriteLine("insert session fail...");
                Environment.Exit(0);
            }
        }
        public void CloseSession(int sessionID)
        {
            User user;
            if( m_users.TryRemove(sessionID, out user) )
            { 
                LogoutUser(user);           
            }
            else
            {
                Console.WriteLine("session closed fail.....");
                Environment.Exit(0);
            }
        }
        public void HandlePacket(int sessionID, dynamic packet)
        {
            User user;
            if (m_users.TryGetValue(sessionID, out user) )
            {
                HandlePacket(user, packet);
            }
        }

        public void LoginUser(User user)
        {
            bool ret = m_usersByName.TryAdd(user.UserData.userName, user);
            if( !ret)
            {
                Console.WriteLine("LoginUser fail.....");
                Environment.Exit(0);
            }
        }
        public void LogoutUser(User user)
        {
            bool ret = m_usersByName.TryRemove(user.UserData.userName, out user);
            if (!ret)
            {
                Console.WriteLine("LogoutUser fail.....");
                Environment.Exit(0);
            }

            RedisManager.SetUserData(user.UserData);
        }

        public void AddRoomData(RoomData roomData)
        {
            bool ret = m_rooms.TryAdd(roomData.roomName, roomData);
            if (!ret)
            {
                Console.WriteLine("AddRoomData fail.....");
                Environment.Exit(0);
            }
        }

        public void HandlePacket(User user, TextMessagePacket packet)
        {
            Console.WriteLine($"{user.SessionID} Handle TextMessagePacket : {packet.msg}");
            m_baseServer.SendPacket(user.SessionID, packet);
        }

        List<RoomData> GetUserRoomData(UserData userData)
        {
            List<RoomData> roomList = new List<RoomData>();
            RoomData roomData;
            // get room
            foreach (var roomName in userData.roomNames)
            {
                if (!m_rooms.ContainsKey(roomName))
                {
                    roomData = new RoomData();
                    RedisManager.GetRoomData(roomName, out roomData);
                    AddRoomData(roomData);
                }
                else
                {
                    m_rooms.TryGetValue(roomName, out roomData);
                }

                roomList.Add(roomData);
            }

            return roomList;
        }

        public void HandlePacket(User user, CQ_LoginPacket packet)
        {
            UserData userData = new UserData();
            bool isNew;

            // login.
            RedisManager.LoginUser(packet.userName, out userData, out isNew);
            user.UserData = userData;

            LoginUser(user);

            SA_LoginPacket loginPacket = new SA_LoginPacket();
            loginPacket.userName = userData.userName;
            loginPacket.result = isNew ? 1 : 0;

            m_baseServer.SendPacket(user.SessionID, loginPacket);

            SN_RoomListPacket roomListPacket = new SN_RoomListPacket();
            roomListPacket.roomList = GetUserRoomData(userData);
            roomListPacket.serverRoomList = m_rooms.Values.ToList();
            m_baseServer.SendPacket(user.SessionID, roomListPacket);
        }

        public void HandlePacket(User user, CQ_RoomListPacket packet)
        {
            SN_RoomListPacket roomListPacket = new SN_RoomListPacket();
            roomListPacket.roomList = GetUserRoomData(user.UserData);
            roomListPacket.serverRoomList = m_rooms.Values.ToList();
            m_baseServer.SendPacket(user.SessionID, roomListPacket);
        }
        public void HandlePacket(User user, CQ_MakeRoomPacket packet)
        {
            SA_MakeRoomPacket makeRoomPacket = new SA_MakeRoomPacket();
            
            if( m_rooms.ContainsKey(packet.roomName))
            {
                makeRoomPacket.result = -1;
            }
            else
            {
                RoomData roomData = new RoomData();
                roomData.roomName = packet.roomName;
                roomData.roomUsers = new List<UserData>() { user.UserData };
                roomData.roomChats = new List<ChatData>();

                AddRoomData(roomData);
                RedisManager.SetRoomData(roomData);

                makeRoomPacket.result = 0;
                makeRoomPacket.room = roomData;
            }

            m_baseServer.SendPacket(user.SessionID, packet);
        }
        public void HandlePacket(User user, CQ_EnterRoomPacket packet)
        {
            SA_EnterRoomPacket enterRoomPacket = new SA_EnterRoomPacket();            
            RoomData roomData;

            bool ret = m_rooms.TryGetValue(packet.roomName, out roomData);
            if(ret)
            {
                roomData.EnterRoom(user.UserData);
                enterRoomPacket.room = roomData;
                enterRoomPacket.result = 0;

                RedisManager.SetRoomData(roomData);
            }
            else
            {
                enterRoomPacket.result = -1;
            }
            m_baseServer.SendPacket(user.SessionID, packet);
        }
        public void HandlePacket(User user, CQ_ChatPacket packet)
        {
            RoomData roomData;
            if( m_rooms.TryGetValue(packet.roomName, out roomData) )
            {
                SN_ChatPacket chatPacket = new SN_ChatPacket();
                ChatData chatData = new ChatData();
                chatData.created_at = DateTime.Now;
                chatData.userName = user.UserData.userName;
                chatData.message = packet.message;

                roomData.Chat(chatData);
                RedisManager.SetRoomData(roomData);

                chatPacket.chat = chatData;
                chatPacket.roomName = roomData.roomName;

                User roomUser;
                foreach(var userData in roomData.roomUsers)
                {
                    if( m_usersByName.TryGetValue(userData.userName, out roomUser) )
                    {
                        m_baseServer.SendPacket(roomUser.SessionID, chatPacket);
                    }
                }                
            }            
        }
    }
}
