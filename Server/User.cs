﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common.Packet;

namespace Server
{
    public class User
    {
        public int SessionID { get; set; }
        public UserData UserData { get; set; }

        public User(int sessionID)
        {
            SessionID = sessionID;
        }
    }
}
