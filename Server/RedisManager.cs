﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StackExchange.Redis;
using Newtonsoft.Json;

using Common.Packet;

namespace Server
{
    public class RedisManager
    {
        ConnectionMultiplexer redis;
        IDatabase db;

        public RedisManager()
        {
        }
        public void InitRedisManager(string connectString)
        {
            redis = ConnectionMultiplexer.Connect("localhost");
            db = redis.GetDatabase();
        }

        public void LoginUser(string userName, out UserData userData, out bool isNew)
        {
            userData.userName = userName;
            
            string key = $"user:{userName}";
            if( db.KeyExists(key) )
            {
                userData = (UserData)JsonConvert.DeserializeObject<UserData>(db.StringGet(key));
                isNew = false;
            }            
            else
            {
                db.StringSet(key, JsonConvert.SerializeObject(userData));
                isNew = true;
            }            
        }

        public void GetRoomData(string roomName, out RoomData roomData)
        {
            string key = $"room:{roomName}";
            string roomStream = db.StringGet(key);
            roomData = JsonConvert.DeserializeObject<RoomData>(roomStream);
        }

        public void SetUserData(UserData userData)
        {
            string key = $"user:{userData.userName}";
            db.StringSet(key, JsonConvert.SerializeObject(userData));
        }

        public void SetRoomData(RoomData roomData)
        {
            string key = $"room:{roomData.roomName}";
            db.StringSet(key, JsonConvert.SerializeObject(roomData));
        }
    }
}
